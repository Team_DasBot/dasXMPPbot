#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import GlobalConfig as conf
from MessageInfo import MessageInfo
import pdb

from sleekxmpp import ClientXMPP
from sleekxmpp.exceptions import IqError, IqTimeout

class XMPPBot(ClientXMPP):

    def __init__(self, jid, password, nick, rooms, **kwargs):
        ClientXMPP.__init__(self, jid, password)

        self.nick = nick
        self.rooms = rooms
        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("message", self.message)
        self.add_event_handler("groupchat_message", self.muc_message)

        # If you wanted more functionality, here's how to register plugins:
        # self.register_plugin('xep_0030') # Service Discovery
        # self.register_plugin('xep_0199') # XMPP Ping

        # Here's how to access plugins once you've registered them:
        # self['xep_0030'].add_feature('echo_demo')

        # If you are working with an OpenFire server, you will
        # need to use a different SSL version:
        # import ssl
        # self.ssl_version = ssl.PROTOCOL_SSLv3

    def session_start(self, event):
        self.send_presence()
        self.get_roster()

        for room in self.rooms:
            self.plugin['xep_0045'].joinMUC(room, self.nick, wait=True)

        # Most get_*/set_* methods from plugins use Iq stanzas, which
        # can generate IqError and IqTimeout exceptions
        #
        # try:
        #     self.get_roster()
        # except IqError as err:
        #     logging.error('There was an error getting the roster')
        #     logging.error(err.iq['error']['condition'])
        #     self.disconnect()
        # except IqTimeout:
        #     logging.error('Server is taking too long to respond')
        #     self.disconnect()


    def muc_message(self, msg):
        if len(msg['body'].strip()) == 0: return
        room = "".join(msg['from'].bare)
        nick = msg['from'].resource
        message_info = MessageInfo(room, 'groupchat', msg['body'], nick)

        if nick != self.nick:
            if msg['body'][0] == conf.COMMAND_CHAR and len(msg['body']) > 1:
                pros = msg['body'][1:].split()
                self.cmd(pros[0], " ".join(pros[1:]).strip(), room, from_nick=nick, mi=message_info, admin=False)
            else:
                self.listen(None, msg['body'].strip(), room, from_nick=nick, mi=message_info)

    def cmd(self, command, args, channel, **kwargs):
        if kwargs['admin']:
            if command == 'join' and args != None and " " not in args and "@" in args:
                self.plugin['xep_0045'].joinMUC(args, self.nick, wait=True)
            elif command == 'part' and args != None and " " not in args:
                self.plugin['xep_0045'].leaveMUC(args, self.nick, msg="I'm out!")

    def listen(self, command, msg, channel, **kwargs): pass

    def msg(self, channel, message, **kwargs):
        message_info = kwargs['mi']
        print(message_info.stream)
        if 'to' in kwargs:
            self.send_message(mto=message_info.stream,
                              mbody='{}: {}'.format(kwargs['to'], message),
                              mtype=message_info.message_type)
        else:
            self.send_message(mto=message_info.stream,
                              mbody=message,
                              mtype=message_info.message_type)

    def notify(self, message, message_info):
        self.msg(None, message, mi=message_info)

    def message(self, msg):
	if msg['type'] != 'chat': return
        room = "".join(msg['from'].bare)
        nick = msg['from'].user
        jid = msg['from'].bare
        message_info = MessageInfo(room, 'chat', msg['body'], nick)

        if nick != self.nick:
            if msg['body'][0] == conf.COMMAND_CHAR and len(msg['body']) > 1:
                pros = msg['body'][1:].split()
                args = " ".join(pros[1:]).strip()
                if len(args) == 0:
                    args = None
                self.cmd(pros[0], args, room, from_nick=nick, mi=message_info, admin=(jid in conf.ADMINS))
            else:
                self.listen(None, msg['body'].strip(), room, from_nick=nick, mi=message_info)
