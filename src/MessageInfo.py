# -*- coding: utf-8 -*-

class MessageInfo:

    def __init__(self, _stream, _type, _message, _nick):
        self.stream = _stream
        self.from_nick = _nick
        self.message_type = _type
        self.message = _message

    def nick(self):
        self.from_nick
