# -*- coding: utf-8 -*-
#
# Copyright (C) 2012 Herman Torjussen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://gnu.org/licenses/>.

from random import randint
import htmlentitydefs
import BeautifulSoup
import urllib2
import urlparse
import re
from os.path import dirname
from os import unlink
from ConfigParser import ConfigParser

#### Taken from Stack Overflow
#### Distributed under CC: Attributed Share Alike
#### user: bobince
def urlEncodeNonAscii(b):
    return re.sub('[\x80-\xFF]', lambda c: '%%%02x' % ord(c.group(0)), b)

def iriToUri(iri):
    parts= urlparse.urlparse(iri)
    return urlparse.urlunparse(
        part.encode('idna') if parti==1 else urlEncodeNonAscii(part.encode('utf-8'))
        for parti, part in enumerate(parts)
    )
#### End taken from Stack Overflow

class Plugin(object):

    def __init__(self, **kwargs):
        self.url_regex = r"https?://(?P<domain>([^.]+.[^.]+)(.[^.]+)*)(/([^/]+/)*([^.]+.[^?]+)?([?]\S+)?)?"
        self.blank_regex = r"\s+"
        self.MAXLINEWIDTH = 1024
        self.ure = re.compile(self.url_regex)
        self.titlesub = re.compile(self.blank_regex)
        self.config = kwargs['config']
        self._generate_blocklist()

    def _generate_blocklist(self):
        re_builder = ur''
        for url in self.config.get('blocklist', 'domains').split('\n'):
            re_builder += url.strip().replace('.', '\.').replace('*', '.*') + "|"
        re_builder = re_builder[:-1]
        print(re_builder)
        self.blocklist = re.compile(re_builder)

    def cmd(self, cmd, args, channel, **kwargs):
        if 'admin' in kwargs and kwargs['admin']:
            if cmd == 'urlblock':
                if args.find(' ') >= 0:
                    args = args.strip().split()[0]
                self.config.set('blocklist', 'domains', self.config.get('blocklist', 'domains') + "\n" + args)
                cfg_file = dirname(__file__) + "/plugin.cfg"
                unlink(cfg_file)
                self.config.write(open(cfg_file, 'w'))
                self._generate_blocklist()
                return [(0, channel, '"{}" is blocked now'.format(args))]

    def listen(self, msg, channel, **kwargs):
        chanurl = self.ure.search(msg)
        if chanurl:
            title = self.urltitle(chanurl)
            if title:
                return [(1, channel, title[:self.MAXLINEWIDTH])]

    def urltitle(self, matchobj):
        url = matchobj.group()
        url = url.decode("utf-8")
        if self.blocklist.match(url) or self.blocklist.match(matchobj.group('domain')):
            return None
        url = iriToUri(url)
        req = urllib2.Request(url, headers={'User-Agent':"Magic Browser"})
        try:
            page_content = urllib2.urlopen(req, timeout=3)
            page_soup = BeautifulSoup.BeautifulSoup(page_content)
        except urllib2.URLError:
            return None
        except:
            print "UrlTitle. Other error"
            return None
        title = None
        if page_soup.title and page_soup.title.string:
            title = page_soup.title.string.strip()
            title = self.titlesub.sub(" ", title)
            title = self.unescape(title).encode("utf-8")
        return title.replace('Trump', 'Drumpf') if title else None


    """
    Fredrik Lundh, 2006
    http://effbot.org/zone/re-sub.htm#unescape-html
    """
    def unescape(self, text):
        def fixup(m):
            text = m.group(0)
            if text[:2] == "&#":
                # character reference
                try:
                    if text[:3] == "&#x":
                        return unichr(int(text[3:-1], 16))
                    else:
                        return unichr(int(text[2:-1]))
                except ValueError:
                    pass
            else:
                # named entity
                try:
                    text = unichr(htmlentitydefs.name2codepoint[text[1:-1]])
                except KeyError:
                    pass
            return text # leave as is
        return re.sub("&#?\w+;", fixup, text)


if __name__ == '__main__':
    config = ConfigParser()
    config.readfp(open("plugin.cfg"))
    p = Plugin(config=config)

    print(p.listen('foo http://bbc.co.uk', '#foobar'))
    print(p.listen('wergujnwerguio https://www.google.com/search?q=fish', '#foobar'))
    print(p.listen('wergujnwerguio https://google.com/search?q=fish', '#foobar'))
    print(p.listen('http://tv.nrk.no', '#foobar'))
    print(p.listen('https://radio.nrk.no/programmer', '#foobar'))
    print(p.listen('https://nrk.no/nordland/_pareidoli_-avgjor-hva-vi-ser-i-bilder-som-dette-1.13712251', '#foobar'))
    print(p.listen('http://www.vg.no/nyheter/utenriks/donald-trump/donald-trump-kan-faa-mer-makt-neste-aar/a/24152944/','#foobar'))
    print(p.listen('https://1.vgc.no/drpublish/images/article/2017/05/11/23995766/1/fact-box-image/josteinbyline.jpg','#foobar'))
    print(p.cmd('urlblock', '*.wikipedia.org', '#foobar', from_user='foo', admin=False))
    print(p.listen('https://en.wikipedia.org/wiki/Michael_Francis_Egan','#foobar'))
    print(p.cmd('urlblock', '*.wikipedia.org', '#foobar', from_user='foo', admin=True))
    print(p.listen('https://en.wikipedia.org/wiki/Michael_Francis_Egan','#foobar'))
